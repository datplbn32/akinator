using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : MonoBehaviour
{
    public string gameId;
    public Test test;

    public void Start()
    {
        StartCoroutine(AwaitStart());
    }

    [Serializable]
    public class Test
    {
        public string question;
        public List<string> answer;
        public string aki_id;
    }

    [Serializable]
    public class TestRespond
    {
        public string aki;
        public int answer;
    }

    public IEnumerator AwaitStart()
    {
        using (UnityWebRequest www = UnityWebRequest.Post("localhost:8080/start", ""))
        {
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
                test = JsonUtility.FromJson<Test>(www.downloadHandler.text);
                gameId = test.aki_id;
                Debug.Log("Done Start");
                yield return new WaitForSeconds(5f);
                StartCoroutine(AwaitAwnser());
            }
        }
    }

    public IEnumerator AwaitAwnser()
    {
        string url = "http://localhost:8080/answer";
        var testRespond = new TestRespond()
        {
            aki = "OKE",
            answer = 0
        };
        string jsonPayload = JsonUtility.ToJson(testRespond);

        UnityWebRequest www = UnityWebRequest.Post(url, jsonPayload);
        www.SetRequestHeader("Content-Type", "application/json");
        www.uploadHandler.contentType = "application/json";
        Debug.Log(www.url);
        www.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(jsonPayload));

        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("Request failed: " + www.error);
        }
        else
        {
            string responseText = www.downloadHandler.text;
            Debug.Log("Response: " + responseText);
        }
    }
}