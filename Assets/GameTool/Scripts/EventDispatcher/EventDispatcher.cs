using System;
using UnityEngine.Events;

namespace GameTool.Scripts.EventDispatcher
{
    public static class EventDispatcher
    {
        private static readonly UnityAction<object[]>[] Action = new UnityAction<object[]>[Enum.GetNames(typeof(EventID)).Length];

        public static void PostEvent(this object obj, EventID eEventType, params object[] args)
        {
            Action[(int) eEventType]?.Invoke(args);
        }

        public static void RegisterListener(this object obj, EventID eEventType, UnityAction<object[]> action)
        {
            Action[(int) eEventType] += action;
        }

        public static void RemoveListener(this object obj, EventID eEventType, UnityAction<object[]> action)
        {
            Action[(int) eEventType] -= action;
        }
    }

    public enum EventID
    {
        None = 0,
        PauseGame,
        ContinueGame
    }
}