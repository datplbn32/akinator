using System.Collections.Generic;
using GameTool.Scripts;
using GameTool.Scripts.Audio;
using GameToolEditable.Audio;
using GameToolEditable.Data;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace GameTool
{
    public class AudioManager : SingletonMonoBehaviour<AudioManager>
    {
        private MixerAudioManager mixerAudioManager => AudioPlayer.Instance.mixerAudioManager;

        private AudioSource musicSpeaker => AudioPlayer.Instance.musicSpeaker;
        private AudioSource soundSpeaker => AudioPlayer.Instance.soundSpeaker;

        [SerializeField] public AudioAsset Serializers;

        private readonly Dictionary<eMusicName, List<AudioClip>> MusicTracks =
            new Dictionary<eMusicName, List<AudioClip>>();

        private readonly Dictionary<eSoundName, List<AudioClip>> SoundTracks =
            new Dictionary<eSoundName, List<AudioClip>>();

        protected override void Awake()
        {
            base.Awake();
            UpdateKey();
        }

        private void Start()
        {
            ChangeMasterVolume(GameData.Instance.MasterVolume);
            ChangeMusicVolume(GameData.Instance.MusicVolume);
            ChangeSFXVolume(GameData.Instance.SoundFXVolume);
        }
        

        private void UpdateKey()
        {
            MusicTracks.Clear();
            SoundTracks.Clear();
            foreach (var serializer in Serializers.musicAsset)
            {
                if (!MusicTracks.ContainsKey(serializer.key))
                {
                    MusicTracks.Add(serializer.key, serializer.listAudio);
                }
            }

            foreach (var serializer in Serializers.soundAsset)
            {
                if (!SoundTracks.ContainsKey(serializer.key))
                {
                    SoundTracks.Add(serializer.key, serializer.listAudio);
                }
            }
        }

        public void PlayMusic(eMusicName filename, bool loop = true)
        {
#if UNITY_EDITOR
            UpdateMusicEditor(filename);
#endif
            try
            {
                musicSpeaker.clip = (MusicTracks[filename].GetRandom());
                musicSpeaker.Play();
                musicSpeaker.loop = loop;
            }
            catch
            {
                Debug.LogError("Music " + filename + "is missing");
            }
        }

        public bool IsSameMusicClip(AudioClip clip)
        {
            if (musicSpeaker.clip != null)
            {
                return musicSpeaker.clip == clip;
            }

            return false;
        }

        public void Shot(eSoundName filename, float volume = 1)
        {
#if UNITY_EDITOR
            UpdateSoundEditor(filename);
#endif
            try
            {
                soundSpeaker.PlayOneShot(SoundTracks[filename].GetRandom(), volume);
            }
            catch
            {
                Debug.LogError("Sound " + filename + "is missing");
            }
        }

#if UNITY_EDITOR
        public void UpdateSoundEditor(eSoundName filename)
        {
            if (!SoundTracks.ContainsKey(filename))
            {
                Debug.LogError("Sound " + filename + "is adding");
                Serializers.soundAsset.Add(new SoundAssetItem
                {
                    key = filename
                });
                Serializers.OnValidate();
                UpdateKey();
                EditorUtility.SetDirty(Serializers);
                AssetDatabase.SaveAssetIfDirty(Serializers);
            }
        }
        
        public void UpdateMusicEditor(eMusicName filename)
        {
            if (!MusicTracks.ContainsKey(filename))
            {
                Debug.LogError("Music " + filename + "is adding");
                Serializers.musicAsset.Add(new MusicAssetItem
                {
                    key = filename
                });
                Serializers.OnValidate();
                UpdateKey();
                EditorUtility.SetDirty(Serializers);
                AssetDatabase.SaveAssetIfDirty(Serializers);
            }
        }
#endif

        public void Shot(AudioClip clip, float volume = 1)
        {
            soundSpeaker.PlayOneShot(clip, volume);
        }

        public void ShotWithIndex(eSoundName filename, int index = 0, float volume = 1f)
        {
#if UNITY_EDITOR
            UpdateSoundEditor(filename);
#endif
            try
            {
                soundSpeaker.PlayOneShot(SoundTracks[filename][index], volume);
            }
            catch
            {
                Debug.LogError("Sound " + filename + "is missing");
            }
        }

        public void Fade(float volume, float duration = 1f)
        {
            if (GameData.Instance.Music)
            {
                mixerAudioManager.FadeMusicVolume(volume, duration);
            }
        }

        public void PauseMusic()
        {
            musicSpeaker.Pause();
        }

        public void StopMusic()
        {
            musicSpeaker.Stop();
        }

        public void ResumeMusic()
        {
            musicSpeaker.UnPause();
        }

        public void StopSFX()
        {
            soundSpeaker.Stop();
        }

        public AudioClip GetAudioClip(eMusicName s)
        {
#if UNITY_EDITOR
            UpdateMusicEditor(s);
#endif
            try
            {
                return MusicTracks[s].GetRandom();
            }
            catch
            {
                Debug.LogError("Music " + s + "is missing");
                return MusicTracks.GetRandom().Value.GetRandom();
            }
        }

        public AudioClip GetAudioClip(eSoundName s)
        {
#if UNITY_EDITOR
            UpdateSoundEditor(s);
#endif
            try
            {
                return SoundTracks[s].GetRandom();
            }
            catch
            {
                Debug.LogError("Sound " + s + "is missing");
                return SoundTracks.GetRandom().Value.GetRandom();
            }
        }

        public void Mute(bool value)
        {
            GameData.Instance.MuteAll = value;

            SetMasterVolume(value ? 0f : 1f);
        }

        public void SetMasterVolume(float value)
        {
            GameData.Instance.MasterVolume = value;
            ChangeMasterVolume(value);
        }

        private void ChangeMasterVolume(float value)
        {
            mixerAudioManager.ChangeMasterVolume(value);
        }

        public void SetMusic(bool value)
        {
            GameData.Instance.Music = value;
            SetMusicVolume(value ? 1f : 0f);
        }

        public void SetMusicVolume(float value)
        {
            GameData.Instance.MusicVolume = value;
            ChangeMusicVolume(value);
        }

        private void ChangeMusicVolume(float value)
        {
            mixerAudioManager.ChangeMusicVolume(value);
        }

        public void SetSFX(bool value)
        {
            GameData.Instance.SoundFX = value;
            SetSFXVolume(value ? 1f : 0f);
        }

        public void SetSFXVolume(float value)
        {
            GameData.Instance.SoundFXVolume = value;
            ChangeSFXVolume(value);
        }

        private void ChangeSFXVolume(float value)
        {
            mixerAudioManager.ChangeSFXVolume(value);
        }
    }
}