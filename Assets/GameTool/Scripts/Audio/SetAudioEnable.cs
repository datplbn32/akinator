using GameToolEditable.Audio;
using UnityEngine;

namespace GameTool.Scripts.Audio
{
    public class SetAudioEnable : MonoBehaviour
    {
        [Header("COMPONENT")]
        [SerializeField] AudioObjectController audioControl;
        [SerializeField] eSoundName soundName;
        [SerializeField] bool loop;
    
        private void OnEnable() 
        {
            PlayAudio();
        }

        void PlayAudio()
        {
            if(audioControl)
            {
                audioControl.SetLoop(loop);
                audioControl.ShotAudioClipNoAdd(soundName);
            }
        }
    }
}