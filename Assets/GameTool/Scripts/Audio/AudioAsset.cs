﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameToolEditable.Audio;
using UnityEditor;
using UnityEngine;

namespace GameTool.Scripts.Audio
{
    [CreateAssetMenu(fileName = "AudioAsset", menuName = "ScriptableObject/AudioAsset", order = 0)]
    public class AudioAsset : ScriptableObject
    {
        public List<MusicAssetItem> musicAsset;
        public List<SoundAssetItem> soundAsset;
        
#if UNITY_EDITOR
        [ContextMenu("Re Update")]
        public void OnValidate()
        {
            var table = AssetDatabase.LoadAssetAtPath<AudioTable>("Assets/GameToolPrivateValue/Audio/AudioTable.asset");
            for (int i = 0; i < musicAsset.Count; i++)
            {
                if (musicAsset[i].key == eMusicName.None)
                {
                    musicAsset[i].listAudio.Clear();
                    continue;
                }
                musicAsset[i].listAudio = table.musicTracksSerializers
                    .Find(music => music.key == musicAsset[i].key.ToString()).track.listAudio.ToList();
            }
            
            for (int i = 0; i < soundAsset.Count; i++)
            {
                if (soundAsset[i].key == eSoundName.None)
                {
                    soundAsset[i].listAudio.Clear();
                    continue;
                }
                soundAsset[i].listAudio = table.soundTracksSerializers
                    .Find(sound => sound.key == soundAsset[i].key.ToString()).track.listAudio.ToList();
            }
            EditorUtility.SetDirty(this);
        }
#endif
    }

    [Serializable]
    public class MusicAssetItem
    {
        public eMusicName key;
        public List<AudioClip> listAudio;
    }

    [Serializable]
    public class SoundAssetItem
    {
        public eSoundName key;
        public List<AudioClip> listAudio;
    }
    

}