using UnityEngine;

namespace GameTool.Scripts.Audio
{
    public class AudioPlayer : SingletonMonoBehaviour<AudioPlayer>
    {
        public MixerAudioManager mixerAudioManager;

        public AudioSource musicSpeaker;
        public AudioSource soundSpeaker;
    }
}
