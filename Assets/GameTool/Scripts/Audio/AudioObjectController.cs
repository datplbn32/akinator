using GameTool.Scripts.EventDispatcher;
using GameToolEditable.Audio;
using UnityEngine;

namespace GameTool.Scripts.Audio
{
    public class AudioObjectController : MonoBehaviour
    {
        [SerializeField] AudioSource audioSource;

        private void Start()
        {
            this.RegisterListener(EventID.PauseGame, PauseGameListener);

            this.RegisterListener(EventID.ContinueGame, ContinueGameListener);
        }

        private void OnDestroy()
        {
            this.RemoveListener(EventID.PauseGame, PauseGameListener);
            this.RemoveListener(EventID.ContinueGame, ContinueGameListener);
        }

        void PauseGameListener(object[] arg0)
        {
            audioSource.mute = true;
        }

        void ContinueGameListener(object[] arg0)
        {
            audioSource.mute = false;
        }

        public void SetAudioSource()
        {
            audioSource = GetComponent<AudioSource>();
        }

        public void ShotAudioClip(eSoundName soundName)
        {
            AudioClip clip = AudioManager.Instance.GetAudioClip(soundName);

            if (clip != null)
            {
                audioSource.volume = 1;
                audioSource.clip = clip;
                audioSource.Play();
            }
        }

        public void ShotAudioClip(eSoundName soundName, float volume)
        {
            AudioClip clip = AudioManager.Instance.GetAudioClip(soundName);

            if (clip != null)
            {
                audioSource.volume = volume;
                audioSource.clip = clip;
                audioSource.Play();
            }
        }

        public void ShotAudioClipNoAdd(eSoundName soundName)
        {
            AudioClip clip = AudioManager.Instance.GetAudioClip(soundName);

            if (clip != null)
            {
                audioSource.PlayOneShot(clip);
            }
        }

        public void SetLoop(bool loop)
        {
            audioSource.loop = loop;
        }

        public void StopAudioClip()
        {
            audioSource.Stop();
        }

        public void Play()
        {
            audioSource.Play();
        }
    }
}