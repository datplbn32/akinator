﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
#endif

namespace GameTool.Scripts.Audio
{
    [CreateAssetMenu(fileName = "AudioTable", menuName = "ScriptableObject/AudioTable", order = 0)]
    public class AudioTable : ScriptableObject
    {
        public List<SerializerMusic> musicTracksSerializers = new List<SerializerMusic>();
        public List<SerializerSound> soundTracksSerializers = new List<SerializerSound>();

        /// <summary>
        /// Dict check xem sound có dùng nhiều không?
        /// </summary>
        private Dict<AudioClip, bool> dictCheck = new Dict<AudioClip, bool>();

        public void Improve()
        {
            List<AudioClip> listAudioClip = new List<AudioClip>();
            List<string> filePaths = new List<string>();
            dictCheck.Clear();

            for (int i = 0; i < musicTracksSerializers.Count; i++)
            {
                for (int j = 0; j < musicTracksSerializers[i].track.listAudio.Count; j++)
                {
                    if (!listAudioClip.Contains(musicTracksSerializers[i].track.listAudio[j]))
                    {
                        listAudioClip.Add(musicTracksSerializers[i].track.listAudio[j]);
                    }
                }
            }

            for (int i = 0; i < soundTracksSerializers.Count; i++)
            {
                for (int j = 0; j < soundTracksSerializers[i].track.listAudio.Count; j++)
                {
                    if (!listAudioClip.Contains(soundTracksSerializers[i].track.listAudio[j]))
                    {
                        listAudioClip.Add(soundTracksSerializers[i].track.listAudio[j]);
                    }
                }
            }

            for (int i = 0; i < listAudioClip.Count; i++)
            {
                filePaths.Add(AssetDatabase.GetAssetPath(listAudioClip[i]));
            }

            for (int i = 0; i < listAudioClip.Count; i++)
            {
                AudioClip audio = listAudioClip[i];

                string audioName = audio.name;
                string filePath = filePaths[i];

                List<IndexOfAudioClip> listIndexOfMusicAudioClipFromTable = new List<IndexOfAudioClip>();
                List<IndexOfAudioClip> listIndexOfSoundAudioClipFromTable = new List<IndexOfAudioClip>();

                if (!filePaths[i].EndsWith(".ogg"))
                {
                    var newpath = Path.ChangeExtension(filePath, "ogg");

                    for (int j = 0; j < musicTracksSerializers.Count; j++)
                    {
                        for (int k = 0; k < musicTracksSerializers[j].track.listAudio.Count; k++)
                        {
                            AudioClip clip = musicTracksSerializers[j].track.listAudio[k];
                            if (clip && clip.name == audioName)
                            {
                                listIndexOfMusicAudioClipFromTable.Add(new IndexOfAudioClip
                                {
                                    indexArray = j,
                                    indexAudio = k
                                });
                            }
                        }
                    }

                    for (int j = 0; j < soundTracksSerializers.Count; j++)
                    {
                        for (int k = 0; k < soundTracksSerializers[j].track.listAudio.Count; k++)
                        {
                            AudioClip clip = soundTracksSerializers[j].track.listAudio[k];
                            if (clip && clip.name == audioName)
                            {
                                listIndexOfSoundAudioClipFromTable.Add(new IndexOfAudioClip
                                {
                                    indexArray = j,
                                    indexAudio = k
                                });
                            }
                        }
                    }

                    File.Move(filePath, newpath);
                    filePath = newpath;
                    AssetDatabase.ImportAsset(filePath, ImportAssetOptions.ForceUpdate);
                }

                audio = (AudioClip) AssetDatabase.LoadAssetAtPath(filePath, typeof(AudioClip));

                for (int j = 0; j < listIndexOfMusicAudioClipFromTable.Count; j++)
                {
                    IndexOfAudioClip indexOfAudio = listIndexOfMusicAudioClipFromTable[j];
                    musicTracksSerializers[indexOfAudio.indexArray].track.listAudio[indexOfAudio.indexAudio] = audio;
                }

                for (int j = 0; j < listIndexOfSoundAudioClipFromTable.Count; j++)
                {
                    IndexOfAudioClip indexOfAudio = listIndexOfSoundAudioClipFromTable[j];
                    soundTracksSerializers[indexOfAudio.indexArray].track.listAudio[indexOfAudio.indexAudio] = audio;
                }
            }

            for (int i = 0; i < musicTracksSerializers.Count; i++)
            {
                for (int j = 0; j < musicTracksSerializers[i].track.listAudio.Count; j++)
                {
                    var audio = musicTracksSerializers[i].track.listAudio[j];
                    string path = AssetDatabase.GetAssetPath(audio);
                    ImproveSetting(path, audio, true, false);
                }
            }

            for (int i = 0; i < soundTracksSerializers.Count; i++)
            {
                for (int j = 0; j < soundTracksSerializers[i].track.listAudio.Count; j++)
                {
                    var audio = soundTracksSerializers[i].track.listAudio[j];
                    string path = AssetDatabase.GetAssetPath(audio);
                    ImproveSetting(path, audio, false, soundTracksSerializers[i].isFrequently);
                }
            }

            EditorUtility.SetDirty(this);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        private void ImproveSetting(string filePath, AudioClip audio, bool isMusic, bool isFrequently)
        {
            if (isMusic)
            {
                if (dictCheck.ContainsKey(audio))
                {
                    return;
                }

                dictCheck.Add(audio, false);
            }
            else
            {
                if (dictCheck.ContainsKey(audio))
                {
                    if (dictCheck[audio])
                    {
                        return;
                    }
                }
                else
                {
                    dictCheck.Add(audio, isFrequently);
                }
            }

            AudioImporter importer = AssetImporter.GetAtPath(filePath) as AudioImporter;

            if (importer != null)
            {
                importer.forceToMono = true;
                if (isFrequently && !isMusic && audio.length < 60)
                {
                    importer.loadInBackground = true;

                    importer.SetOverrideSampleSettings("Android", new AudioImporterSampleSettings
                    {
                        loadType = AudioClipLoadType.DecompressOnLoad,
                        compressionFormat = AudioCompressionFormat.ADPCM,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate,
                    });

                    importer.SetOverrideSampleSettings("iOS", new AudioImporterSampleSettings
                    {
                        loadType = AudioClipLoadType.DecompressOnLoad,
                        compressionFormat = AudioCompressionFormat.ADPCM,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate,
                    });

                    importer.SetOverrideSampleSettings("Standalone", new AudioImporterSampleSettings
                    {
                        loadType = AudioClipLoadType.DecompressOnLoad,
                        compressionFormat = AudioCompressionFormat.ADPCM,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate,
                    });
                }
                else if (!isFrequently && !isMusic && audio.length < 60)
                {
                    importer.loadInBackground = false;

                    importer.SetOverrideSampleSettings("Android", new AudioImporterSampleSettings
                    {
                        loadType = AudioClipLoadType.CompressedInMemory,
                        compressionFormat = AudioCompressionFormat.ADPCM,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate
                    });

                    importer.SetOverrideSampleSettings("iOS", new AudioImporterSampleSettings
                    {
                        loadType = AudioClipLoadType.CompressedInMemory,
                        compressionFormat = AudioCompressionFormat.ADPCM,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate
                    });

                    importer.SetOverrideSampleSettings("Standalone", new AudioImporterSampleSettings
                    {
                        loadType = AudioClipLoadType.CompressedInMemory,
                        compressionFormat = AudioCompressionFormat.ADPCM,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate
                    });
                }
                else
                {
                    importer.loadInBackground = false;

                    importer.SetOverrideSampleSettings("Android", new AudioImporterSampleSettings
                    {
#if UNITY_WEBGL
                        loadType = AudioClipLoadType.CompressedInMemory,
#else
                        loadType = AudioClipLoadType.Streaming,
#endif
                        compressionFormat = AudioCompressionFormat.Vorbis,
                        quality = 100,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate,
                    });

                    importer.SetOverrideSampleSettings("iOS", new AudioImporterSampleSettings
                    {
#if UNITY_WEBGL
                        loadType = AudioClipLoadType.CompressedInMemory,
#else
                        loadType = AudioClipLoadType.Streaming,
#endif
                        compressionFormat = AudioCompressionFormat.Vorbis,
                        quality = 100,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate,
                    });

                    importer.SetOverrideSampleSettings("Standalone", new AudioImporterSampleSettings
                    {
#if UNITY_WEBGL
                        loadType = AudioClipLoadType.CompressedInMemory,
#else
                        loadType = AudioClipLoadType.Streaming,
#endif
                        compressionFormat = AudioCompressionFormat.Vorbis,
                        quality = 100,
                        sampleRateSetting = AudioSampleRateSetting.PreserveSampleRate,
                    });
                }

                Debug.Log(filePath);
                importer.SaveAndReimport();
            }
        }
    }

    public class IndexOfAudioClip
    {
        public int indexArray;
        public int indexAudio;
    }

    [Serializable]
    public class TrackAudio
    {
        public List<AudioClip> listAudio;
    }

    [Serializable]
    public class SerializerMusic
    {
        public string key;
        public TrackAudio track;
    }

    [Serializable]
    public class SerializerSound
    {
        public string key;
        public TrackAudio track;

        [Tooltip(
            @"Âm thanh được phát với số lượng lớn (ví dụ: âm thanh vũ khí, tiếng bước chân, âm thanh va chạm, v.v.). Hoạt động tốt nhất với các cài đặt sau (cũng phù hợp với âm thanh ngắn dưới 10 giây)")]
        public bool isFrequently;
    }
}