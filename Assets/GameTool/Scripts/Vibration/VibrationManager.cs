namespace GameTool.Scripts.Vibration
{
    public class VibrationManager : SingletonMonoBehaviour<VibrationManager>
    {
        private void Start()
        {
            Vibration.Init();
        }
        public void Vibrate(HapticType hapticType = HapticType.None)
        {
            if (GameToolEditable.Data.GameData.Instance.Vibrate)
            {
                switch (hapticType)
                {
                    case HapticType.None:
                    {
                        break;
                    }
                    case HapticType.Sort:
                    {
                        Vibration.VibrateAndroid(50);
                        break;
                    }
                    case HapticType.Medium:
                    {
                        Vibration.VibrateAndroid(90);
                        break;
                    }
                    case HapticType.Hard:
                    {
                        Vibration.VibrateAndroid(120);
                        break;
                    }
                }
            }
        }
    }

    public enum HapticType
    {
        None,
        Sort,
        Medium,
        Hard
    }
}