﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace GameTool.Scripts.UI
{
    [CreateAssetMenu(fileName = "CanvasPrefTable", menuName = "ScriptableObject/CanvasPrefTable", order = 0)]
    public class CanvasPrefTable : ScriptableObject
    {
        public List<UISerializer> Serializers = new List<UISerializer>();
    }
    
    [Serializable]
    public class SettingUI
    {
        public eUIType uiType;
        public BaseUI baseUI;
    }

    [Serializable]
    public class UISerializer
    {
        public string key;
        public SettingUI settingUI;
    }
    
    public enum eUIType
    {
        Menu,
        Popup,
        AlwaysOnTop,
    }
}