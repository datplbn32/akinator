using UnityEngine;

namespace GameTool
{
    public class AutoFindCameraWorldSpace : MonoBehaviour
    {
        [SerializeField] private Canvas _canvas;

        private void OnValidate()
        {
            _canvas = GetComponent<Canvas>();
        }

        private void Awake()
        {
            _canvas.worldCamera = Camera.main;
        }
    }
}