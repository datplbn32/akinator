using System;
using System.Collections.Generic;
using GameToolEditable.Canvas;
using UnityEditor;
using UnityEngine;

namespace GameTool.Scripts.UI
{
    [CreateAssetMenu(fileName = "CanvasPrefAssets", menuName = "ScriptableObject/CanvasPrefAssets", order = 0)]
    public class CanvasPrefAssets : ScriptableObject
    {
        public List<CanvasPrefAssetItem> uiAsset;
        
#if UNITY_EDITOR
        [ContextMenu("Re Update")]
        public void OnValidate()
        {
            var table = AssetDatabase.LoadAssetAtPath<CanvasPrefTable>("Assets/GameToolPrivateValue/Canvas/CanvasPrefTable.asset");
            for (int i = 0; i < uiAsset.Count; i++)
            {
                if (uiAsset[i].key == eUIName.None)
                {
                    uiAsset[i].ui = null;
                    continue;
                }
                uiAsset[i].ui = table.Serializers
                    .Find(ui => ui.key == uiAsset[i].key.ToString()).settingUI.baseUI;
            }
            EditorUtility.SetDirty(this);
        }
#endif
    }
    
    [Serializable]
    public class CanvasPrefAssetItem
    {
        public eUIName key;
        public BaseUI ui;
    }
}
