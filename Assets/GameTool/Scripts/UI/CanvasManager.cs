using System;
using System.Collections.Generic;
using GameToolEditable.Canvas;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
#if UNITY_EDITOR
#endif

namespace GameTool.Scripts.UI
{
    public class CanvasManager : SingletonMonoBehaviour<CanvasManager>
    {
        public Canvas canvas;
        public CanvasScaler canvasScaler;
        public float aspect { get; private set; }

        private readonly Dictionary<eUIName, List<BaseUI>> ListUI = new Dictionary<eUIName, List<BaseUI>>();
        private readonly Dictionary<eUIName, Stack<BaseUI>> DictUIDisabled = new Dictionary<eUIName, Stack<BaseUI>>();
        private readonly Dictionary<eUIType, Transform> DictMenu = new Dictionary<eUIType, Transform>();

        public CanvasPrefAssets Serializers;

        public readonly Dictionary<eUIName, BaseUI> DictUIPref = new Dictionary<eUIName, BaseUI>();

        public eUIName StartMenu
        {
            get
            {
                return startMenu;
            }
            set
            {
                startMenu = value;
            }
        }

        [SerializeField] private eUIName startMenu;

        private void OnValidate()
        {
            canvas = GetComponent<Canvas>();
            canvasScaler = GetComponent<CanvasScaler>();
        }

        protected override void Awake()
        {
            base.Awake();

            UpdateKey();

            aspect = (float)Screen.width / Screen.height;

            for (int i = 0; i < Serializers.uiAsset.Count; i++)
            {
                DictUIDisabled.Add(Serializers.uiAsset[i].key, new Stack<BaseUI>());
                ListUI.Add(Serializers.uiAsset[i].key, new List<BaseUI>());
            }

            for (int i = 0; i < Enum.GetNames(typeof(eUIType)).Length; i++)
            {
                GameObject obj = new GameObject(Enum.GetNames(typeof(eUIType))[i], typeof(RectTransform));
                obj.transform.SetParent(transform);
                obj.transform.position = transform.position;
                obj.transform.localScale = Vector3.one;
                SetFullRect(obj);
                DictMenu.Add((eUIType)i, obj.transform);
            }
        }

        private void Start()
        {
            if (startMenu != eUIName.None)
            {
                Push(startMenu);
            }
        }

        public BaseUI Push(eUIName identifier, object[] args = null)
        {
#if UNITY_EDITOR
            UpdateUIAsset(identifier);
#endif
            BaseUI uiReturn;
            if (DictUIDisabled[identifier].Count > 0)
            {
                uiReturn = DictUIDisabled[identifier].Pop();
                if (uiReturn)
                {
                    GotoTop(uiReturn);
                    uiReturn.gameObject.SetActive(true);
                    uiReturn.Init(args);
                    return uiReturn;
                }
            }

            uiReturn = Instantiate(DictUIPref[identifier], DictMenu[DictUIPref[identifier].uiType]);
            ListUI[identifier].Add(uiReturn);
            uiReturn.uiName = identifier;
            uiReturn.uiType = DictUIPref[identifier].uiType;
            GotoTop(uiReturn);
            uiReturn.Init(args);
            return uiReturn;
        }

        public void Pop(BaseUI ui)
        {
            ui.gameObject.SetActive(false);
            DictUIDisabled[ui.uiName].Push(ui);
        }

        public void Pop(eUIName identifier)
        {
#if UNITY_EDITOR
            UpdateUIAsset(identifier);
#endif
            var ui = ListUI[identifier].Find(_ui => _ui.uiName == identifier && _ui.gameObject.activeSelf);
            if (ui)
            {
                ui.Pop();
            }
        }

        public void Pop(eUIType identifier)
        {
            for (int i = 0; i < ListUI.Count; i++)
            {
                BaseUI popup = ListUI[(eUIName)i].Find(ui => ui.uiType == identifier && ui.gameObject.activeSelf);

                if (popup)
                {
                    popup.Pop();
                }
            }
        }

        public BaseUI FindEnabling(eUIName identifier)
        {
#if UNITY_EDITOR
            UpdateUIAsset(identifier);
#endif
            return ListUI[identifier].Find(ui => ui.uiName == identifier && ui.gameObject.activeSelf);
        }

        public bool IsShowing(eUIName identifier)
        {
#if UNITY_EDITOR
            UpdateUIAsset(identifier);
#endif
            BaseUI ui = FindEnabling(identifier);
            return ui != null;
        }

        public bool IsHavePopupShowing()
        {
            for (int i = 0; i < ListUI.Count; i++)
            {
                BaseUI popup = ListUI[(eUIName)i].Find(ui => ui.uiType == eUIType.Popup && ui.gameObject.activeSelf);

                if (popup)
                {
                    return true;
                }
            }

            return false;
        }

        private void GotoTop(BaseUI baseUi)
        {
            baseUi.transform.SetSiblingIndex(baseUi.transform.parent.childCount - 1);
        }

        private void SetFullRect(GameObject _obj)
        {
            RectTransform _rect = _obj.GetComponent<RectTransform>();

            _rect.anchorMin = Vector2.zero;
            _rect.anchorMax = Vector2.one;

            _rect.pivot = Vector2.one / 2;

            _rect.offsetMin = Vector2.zero;
            _rect.offsetMax = Vector2.zero;
        }


        public void UpdateKey()
        {
            DictUIPref.Clear();
            foreach (var serializer in Serializers.uiAsset)
            {
                DictUIPref.Add(serializer.key, serializer.ui);
            }
        }
        
#if UNITY_EDITOR
        public void UpdateUIAsset(eUIName filename)
        {
            if (!DictUIPref.ContainsKey(filename))
            {
                Serializers.uiAsset.Add(new CanvasPrefAssetItem
                {
                    key = filename,
                });
                Serializers.OnValidate();
                UpdateKey();
                ReSetup();
                EditorUtility.SetDirty(Serializers);
                AssetDatabase.SaveAssetIfDirty(Serializers);
            }
        }
        
        private void ReSetup()
        {
            for (int i = 0; i < Serializers.uiAsset.Count; i++)
            {
                if (DictUIDisabled.ContainsKey(Serializers.uiAsset[i].key))
                {
                    continue;
                }
                DictUIDisabled.Add(Serializers.uiAsset[i].key, new Stack<BaseUI>());
                ListUI.Add(Serializers.uiAsset[i].key, new List<BaseUI>());
            }
        }
#endif
    }
}