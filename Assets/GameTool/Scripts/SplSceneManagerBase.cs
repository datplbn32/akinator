using System.Collections;
using UnityEngine;

namespace GameTool.Scripts
{
    public class SplSceneManagerBase : SingletonMonoBehaviour<SplSceneManagerBase>
    {
        protected virtual void Start()
        {
            StartCoroutine(nameof(LoadSceneHome));
        }

        protected virtual IEnumerator LoadSceneHome()
        {
            yield return new WaitForSeconds(3);
        }
    }
}