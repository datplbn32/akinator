﻿using System.Collections.Generic;
using System.IO;
using GameTool.Scripts.Pool;
using UnityEditor;
using UnityEngine;

namespace GameTool.Scripts.Editor
{
    [CustomEditor(typeof(PoolingTable))]
    public class PoolingManagerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            PoolingTable instance = (PoolingTable) target;
            base.OnInspectorGUI();
            if (GUILayout.Button("Update Enum"))
            {               
                string path = Application.dataPath + "/GameToolPrivateValue/Pooling/ePrefabPool.cs";
                List<string> texts = new List<string>();
                texts.Add("namespace GameToolEditable.Data");
                texts.Add("{");
                texts.Add("    public enum ePrefabPool");
                texts.Add("    {");
                texts.Add("        None,");
                foreach (var item in instance.Serializers)
                {
                    if (item.key != "None")
                    {
                        texts.Add("        " + item.key + ",");                       
                    }
                }

                texts.Add("    }");
                texts.Add("}");

                File.WriteAllLines(path, texts.ToArray());
                AssetDatabase.SaveAssets();
                AssetDatabase.ImportAsset(@"Assets/GameToolPrivateValue/Pooling/ePrefabPool.cs");
                AssetDatabase.Refresh();
            }

            if (GUILayout.Button("Update Prefab"))
            {
                instance.UpdatePrefab();
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}