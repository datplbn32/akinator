using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameTool.Scripts.Data;
using GameToolEditable.Data;
using UnityEditor;
using UnityEngine;

namespace GameTool.Scripts.Editor
{
    [CustomEditor(typeof(GameData))]
    public class GameDataEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Load Data"))
            {
                LoadData();
            }

            if (GUILayout.Button("Save Data"))
            {
                SaveData();
            }

            if (GUILayout.Button("Clear Data"))
            {
                ClearData();
            }

            GUILayout.EndHorizontal();

            if (GUILayout.Button("Update Script"))
            {
                UpdateScript();
            }

            base.OnInspectorGUI();
        }

        public static void UpdateScript()
        {
            string path = Application.dataPath + "/GameToolPrivateValue/GameData/GameDataControl.cs";

            string pathEnum = Application.dataPath + "/GameToolPrivateValue/GameData/eData.cs";

            List<string> fieldName = typeof(GameDataSave).GetFields().Select(field => field.Name).ToList();

            List<string> textEnum = new List<string>();
            textEnum.Add("namespace GameToolEditable.Data");
            textEnum.Add("{");
            textEnum.Add("    public enum eData");
            textEnum.Add("    {");
            textEnum.Add("        None,");

            List<string> texts = new List<string>();
            texts.Add("namespace GameToolEditable.Data");
            texts.Add("{");
            texts.Add("    public static class GameDataControl");
            texts.Add("    {");
            texts.Add("        public static void SaveAllData()");
            texts.Add("        {");

            foreach (string value in fieldName)
            {
                texts.Add(string.Format("            SaveGameData.SaveData(eData.{0}, GameData.Instance.Data.{0});",
                    value));
                textEnum.Add("        " + value + ",");
            }

            textEnum.Add("    }");
            textEnum.Add("}");

            texts.Add("        }");

            texts.Add("");

            texts.Add("        public static void LoadAllData()");
            texts.Add("        {");

            foreach (string value in fieldName)
            {
                texts.Add(string.Format("            SaveGameData.LoadData(eData.{0}, ref GameData.Instance.Data.{0});",
                    value));
            }

            texts.Add("        }");

            texts.Add("    }");

            texts.Add("}");

            File.WriteAllLines(path, texts.ToArray());
            File.WriteAllLines(pathEnum, textEnum.ToArray());
            AssetDatabase.ImportAsset(@"Assets/GameToolPrivateValue/GameData/GameDataControl.cs");
            AssetDatabase.ImportAsset(@"Assets/GameToolPrivateValue/GameData/eData.cs");
            AssetDatabase.Refresh();
        }

        public void SaveData()
        {
            var gameData = target as GameData;
            if (gameData)
            {
                gameData.SaveAllData();
            }
        }

        public void LoadData()
        {
            var gameData = target as GameData;
            if (gameData)
            {
                gameData.LoadAllData();
            }
        }

        public void ClearData()
        {
            SaveGameManager.DeleteAllSave();
            var gameData = target as GameData;
            if (gameData)
            {
                gameData.ClearData();
            }
        }
    }
}