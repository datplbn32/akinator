﻿using System.Collections.Generic;
using System.IO;
using GameTool.Scripts.Audio;
using UnityEditor;
using UnityEngine;

namespace GameTool.Scripts.Editor
{
    [CustomEditor(typeof(AudioTable))]
    public class AudioManagerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            AudioTable instance = (AudioTable) target;
            base.OnInspectorGUI();
            if (GUILayout.Button("Update Script"))
            {
                string path = Application.dataPath + "/GameToolPrivateValue/Audio/eAudioName.cs";
                List<string> texts = new List<string>();
                texts.Add("namespace GameToolEditable.Audio");
                texts.Add("{");
                texts.Add("    public enum eMusicName");
                texts.Add("    {");
                texts.Add("        None,");
                foreach (var item in instance.musicTracksSerializers)
                {
                    if (item.key != "None")
                    {
                        texts.Add("        " + item.key + ",");
                    }
                }

                texts.Add("    }");
                texts.Add("    public enum eSoundName");
                texts.Add("    {");
                texts.Add("        None,");
                foreach (var item in instance.soundTracksSerializers)
                {
                    if (item.key != "None")
                    {
                        texts.Add("        " + item.key + ",");
                    }
                }

                texts.Add("    }");
                texts.Add("}");
                File.WriteAllLines(path, texts.ToArray());
                AssetDatabase.ImportAsset(@"Assets/GameToolPrivateValue/Audio/eAudioName.cs");
                AssetDatabase.Refresh();
            }

            if (GUILayout.Button("Improve Audios"))
            {
                instance.Improve();
                AssetDatabase.Refresh();
            }
        }
    }
}