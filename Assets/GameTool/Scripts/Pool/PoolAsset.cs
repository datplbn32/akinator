﻿using System;
using System.Collections.Generic;
using System.Linq;
using GameToolEditable.Pooling;
using UnityEditor;
using UnityEngine;

namespace GameTool.Scripts.Pool
{
    [CreateAssetMenu(fileName = "PoolAsset", menuName = "ScriptableObject/PoolAsset", order = 0)]
    public class PoolAsset : ScriptableObject
    {
        public List<PoolAssetItem> poolAsset;
        
#if UNITY_EDITOR
        [ContextMenu("Re Update")]
        public void OnValidate()
        {
            var table = AssetDatabase.LoadAssetAtPath<PoolingTable>("Assets/GameToolPrivateValue/Pooling/PoolingTable.asset");
            for (int i = 0; i < poolAsset.Count; i++)
            {
                if (poolAsset[i].key == ePrefabPool.None)
                {
                    poolAsset[i].ListPooling.Clear();
                    continue;
                }
                poolAsset[i].ListPooling = table.Serializers
                    .Find(music => music.key == poolAsset[i].key.ToString()).ItemPooling.listPooling.ToList();
            }
            EditorUtility.SetDirty(this);
        }
#endif
    }
    
    [Serializable]
    public class PoolAssetItem
    {
        public ePrefabPool key;
        public List<BasePooling> ListPooling;
        public int amount;
    }
}