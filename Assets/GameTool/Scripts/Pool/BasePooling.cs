﻿using System;
using System.Collections;
using GameToolEditable.Pooling;
using UnityEngine;

namespace GameTool.Scripts.Pool
{
    public class BasePooling : MonoBehaviour
    {
        [NonSerialized] public ePrefabPool poolName = ePrefabPool.None;
        public string poolNameString;
        public int index;
        Transform parrentTrans;

        public virtual void Init(params object[] args)
        {
        }
        
        public void SetParrentTrans(Transform trans)
        {
            parrentTrans = trans;
        }

        public void ResetToParrent()
        {
            transform.parent = parrentTrans;
        }

        public virtual void Disable()
        {
            if (this)
            {
                gameObject.SetActive(false);
            }
        }

        public virtual void Disable(float time)
        {
            StartCoroutine(nameof(WaitDisable), time);
        }

        private IEnumerator WaitDisable(float time)
        {
            yield return new WaitForSeconds(time);
            Disable();
        }

        protected virtual void OnDisable()
        {
            if (PoolingManager.IsInstanceValid() && this)
            {
                PoolingManager.Instance.PushToPooler(this);
            }
        }

        public void CheckEnum()
        {
            if (poolName.ToString() != poolNameString)
            {
                poolName = (ePrefabPool)Enum.Parse(typeof(ePrefabPool), poolNameString);
            }
        }
    }
}