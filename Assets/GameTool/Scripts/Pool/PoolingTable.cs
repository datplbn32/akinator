﻿using System;
using System.Collections.Generic;
using GameToolEditable.Pooling;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR

#endif

namespace GameTool.Scripts.Pool
{
    [CreateAssetMenu(fileName = "PoolingTable", menuName = "ScriptableObject/PoolingTable", order = 0)]
    public class PoolingTable : ScriptableObject
    {
        public List<PoolSerializer> Serializers = new List<PoolSerializer>();

        public void UpdatePrefab()
        {
            foreach (var item in Serializers)
            {
                if (item.key != "None")
                {
                    for (int i = 0; i < item.ItemPooling.listPooling.Count; i++)
                    {
                        BasePooling obj = item.ItemPooling.listPooling[i];
                        if (obj)
                        {
                            ePrefabPool prefName;
                            if (Enum.TryParse(item.key, out prefName))
                            {
                                obj.poolName = prefName;
                                obj.poolNameString = prefName.ToString();
                            }
                            obj.index = i;

#if UNITY_EDITOR
                            if (PrefabUtility.IsPartOfRegularPrefab(obj))
                            {
                                EditorUtility.SetDirty(obj);
                                PrefabUtility.RecordPrefabInstancePropertyModifications(obj.gameObject);
                            }
#endif
                        }
                    }
                }
            }
        }
    }
    
    [Serializable]
    public class ItemPooling
    {
        public List<BasePooling> listPooling;
    }

    [Serializable]
    public class PoolSerializer
    {
        public string key;
        public ItemPooling ItemPooling;
    }
}