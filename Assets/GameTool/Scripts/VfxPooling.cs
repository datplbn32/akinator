using GameTool.Scripts.Pool;
using UnityEngine;

namespace GameTool.Scripts
{
    public class VfxPooling : BasePooling
    {
        [SerializeField] private ParticleSystem particle;

        private void OnValidate()
        {
            if (!particle)
            {
                particle = GetComponent<ParticleSystem>();
            }
            if (!particle)
            {
                particle = GetComponentInChildren<ParticleSystem>();
            }
        }

        public override void Init(params object[] args)
        {
            base.Init(args);
            particle.Play(true);
        }
    }
}
