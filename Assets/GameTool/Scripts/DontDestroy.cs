namespace GameTool.Scripts
{
    public class DontDestroy : SingletonMonoBehaviour<DontDestroy>
    {
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
    }
}
