using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GameTool.Scripts.Joystick
{
    public class Joystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler
    {
        [SerializeField] private RectTransform pressArea;
        [SerializeField] private Image backPanel;
        [SerializeField] private Image knob;
        [SerializeField] private float maxDistance;
        private Vector2 oldAnchorPos;

        public Vector3 InputDirection { get; private set; }

        private void Awake()
        {
            oldAnchorPos = backPanel.rectTransform.anchoredPosition;
            InputDirection = Vector3.zero;
            knob.rectTransform.anchoredPosition = Vector3.zero;
        }

        public virtual void OnDrag(PointerEventData pointerEventData)
        {
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(backPanel.rectTransform,
                pointerEventData.position,
                pointerEventData.pressEventCamera, out var position))
            {
                if (position.magnitude > maxDistance)
                {
                    position = position.normalized * maxDistance;
                }

                knob.rectTransform.anchoredPosition = position;
                InputDirection = position / maxDistance;
            }
        }

        public virtual void OnPointerDown(PointerEventData pointerEventData)
        {
            RectTransformUtility.ScreenPointToLocalPointInRectangle(pressArea, pointerEventData.position,
                pointerEventData.pressEventCamera, out var position);
            position.x += (0.5f - backPanel.rectTransform.anchorMin.x) * pressArea.rect.width;
            position.y += (0.5f - backPanel.rectTransform.anchorMin.y) * pressArea.rect.height;
            backPanel.rectTransform.anchoredPosition = position;
            OnDrag(pointerEventData);
        }

        public virtual void OnPointerUp(PointerEventData pointerEventData)
        {
            backPanel.rectTransform.anchoredPosition = oldAnchorPos;
            InputDirection = Vector3.zero;
            knob.rectTransform.anchoredPosition = Vector3.zero;
        }
    }
}