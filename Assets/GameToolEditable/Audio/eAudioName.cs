namespace GameToolEditable.Audio
{
    public enum eMusicName
    {
        None,
        BgMenu,
        BgGameplay,
    }
    public enum eSoundName
    {
        None,
        ButtonClick,
        CollectCoin,
        CollectDiamond,
        Lose,
        Victory,
        SuccessReward,
        Tick,
    }
}
