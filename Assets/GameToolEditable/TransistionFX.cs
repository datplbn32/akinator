﻿using System.Collections;
using DG.Tweening;
using GameTool.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace GameToolEditable
{
    public class TransistionFX : SingletonMonoBehaviour<TransistionFX>
    {
        [SerializeField] private Image dim;
        public IEnumerator OnLoadScene()
        {
            dim.gameObject.SetActive(true);
            dim.DOFade(1, 0.5f);
            yield return new WaitForSeconds(0.5f);
        }

        public void EndLoadScene()
        {
            dim.DOFade(0, 0.25f).OnComplete(() =>
            {
                dim.gameObject.SetActive(false);
            });
        }
    }
}