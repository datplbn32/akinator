namespace GameToolEditable.Data
{
    public static class GameDataControl
    {
        public static void SaveAllData()
        {
            GameData.Instance.SaveData(eData.FirstOpen, GameData.Instance.Data.FirstOpen);
            GameData.Instance.SaveData(eData.FirstPlay, GameData.Instance.Data.FirstPlay);
            GameData.Instance.SaveData(eData.Rated, GameData.Instance.Data.Rated);
            GameData.Instance.SaveData(eData.RemoveAds, GameData.Instance.Data.RemoveAds);
            GameData.Instance.SaveData(eData.GDPR, GameData.Instance.Data.GDPR);
            GameData.Instance.SaveData(eData.GDPRShowed, GameData.Instance.Data.GDPRShowed);
            GameData.Instance.SaveData(eData.MuteAll, GameData.Instance.Data.MuteAll);
            GameData.Instance.SaveData(eData.Music, GameData.Instance.Data.Music);
            GameData.Instance.SaveData(eData.SoundFX, GameData.Instance.Data.SoundFX);
            GameData.Instance.SaveData(eData.Vibrate, GameData.Instance.Data.Vibrate);
            GameData.Instance.SaveData(eData.MasterVolume, GameData.Instance.Data.MasterVolume);
            GameData.Instance.SaveData(eData.MusicVolume, GameData.Instance.Data.MusicVolume);
            GameData.Instance.SaveData(eData.SoundFXVolume, GameData.Instance.Data.SoundFXVolume);
            GameData.Instance.SaveData(eData.Coin, GameData.Instance.Data.Coin);
            GameData.Instance.SaveData(eData.Diamond, GameData.Instance.Data.Diamond);
            GameData.Instance.SaveData(eData.VictoryCount, GameData.Instance.Data.VictoryCount);
            GameData.Instance.SaveData(eData.LoseCount, GameData.Instance.Data.LoseCount);
            GameData.Instance.SaveData(eData.CurrentLevel, GameData.Instance.Data.CurrentLevel);
            GameData.Instance.SaveData(eData.LevelUnlocked, GameData.Instance.Data.LevelUnlocked);
            GameData.Instance.SaveData(eData.ListLevelUnlockID, GameData.Instance.Data.ListLevelUnlockID);
            GameData.Instance.SaveData(eData.DictLevelPlayInfoData, GameData.Instance.Data.DictLevelPlayInfoData);
            GameData.Instance.SaveData(eData.SkinSelectedID, GameData.Instance.Data.SkinSelectedID);
            GameData.Instance.SaveData(eData.TrySkinSelectedID, GameData.Instance.Data.TrySkinSelectedID);
            GameData.Instance.SaveData(eData.LastSkinSelectedID, GameData.Instance.Data.LastSkinSelectedID);
            GameData.Instance.SaveData(eData.ListSkinUnlockedID, GameData.Instance.Data.ListSkinUnlockedID);
            GameData.Instance.SaveData(eData.ListSkinAdsID, GameData.Instance.Data.ListSkinAdsID);
            GameData.Instance.SaveData(eData.Day, GameData.Instance.Data.Day);
            GameData.Instance.SaveData(eData.FirstDay, GameData.Instance.Data.FirstDay);
            GameData.Instance.SaveData(eData.IndexOfDayDaily, GameData.Instance.Data.IndexOfDayDaily);
            GameData.Instance.SaveData(eData.DayClamReward, GameData.Instance.Data.DayClamReward);
            GameData.Instance.SaveData(eData.StartAngleSpin, GameData.Instance.Data.StartAngleSpin);
            GameData.Instance.SaveData(eData.CanSpin, GameData.Instance.Data.CanSpin);
            GameData.Instance.SaveData(eData.TimeSpin, GameData.Instance.Data.TimeSpin);
            GameData.Instance.SaveData(eData.CountDownTimeSpin, GameData.Instance.Data.CountDownTimeSpin);
            GameData.Instance.SaveData(eData.ListIdSkinSpin, GameData.Instance.Data.ListIdSkinSpin);
            GameData.Instance.SaveData(eData.ProgressSkinVictoryCurrentCount, GameData.Instance.Data.ProgressSkinVictoryCurrentCount);
            GameData.Instance.SaveData(eData.ListIdSkinVictoryProgressDisplayed, GameData.Instance.Data.ListIdSkinVictoryProgressDisplayed);
            GameData.Instance.SaveData(eData.CurrentLanguage, GameData.Instance.Data.CurrentLanguage);
        }

        public static void LoadAllData()
        {
            GameData.Instance.LoadData(eData.FirstOpen, ref GameData.Instance.Data.FirstOpen);
            GameData.Instance.LoadData(eData.FirstPlay, ref GameData.Instance.Data.FirstPlay);
            GameData.Instance.LoadData(eData.Rated, ref GameData.Instance.Data.Rated);
            GameData.Instance.LoadData(eData.RemoveAds, ref GameData.Instance.Data.RemoveAds);
            GameData.Instance.LoadData(eData.GDPR, ref GameData.Instance.Data.GDPR);
            GameData.Instance.LoadData(eData.GDPRShowed, ref GameData.Instance.Data.GDPRShowed);
            GameData.Instance.LoadData(eData.MuteAll, ref GameData.Instance.Data.MuteAll);
            GameData.Instance.LoadData(eData.Music, ref GameData.Instance.Data.Music);
            GameData.Instance.LoadData(eData.SoundFX, ref GameData.Instance.Data.SoundFX);
            GameData.Instance.LoadData(eData.Vibrate, ref GameData.Instance.Data.Vibrate);
            GameData.Instance.LoadData(eData.MasterVolume, ref GameData.Instance.Data.MasterVolume);
            GameData.Instance.LoadData(eData.MusicVolume, ref GameData.Instance.Data.MusicVolume);
            GameData.Instance.LoadData(eData.SoundFXVolume, ref GameData.Instance.Data.SoundFXVolume);
            GameData.Instance.LoadData(eData.Coin, ref GameData.Instance.Data.Coin);
            GameData.Instance.LoadData(eData.Diamond, ref GameData.Instance.Data.Diamond);
            GameData.Instance.LoadData(eData.VictoryCount, ref GameData.Instance.Data.VictoryCount);
            GameData.Instance.LoadData(eData.LoseCount, ref GameData.Instance.Data.LoseCount);
            GameData.Instance.LoadData(eData.CurrentLevel, ref GameData.Instance.Data.CurrentLevel);
            GameData.Instance.LoadData(eData.LevelUnlocked, ref GameData.Instance.Data.LevelUnlocked);
            GameData.Instance.LoadData(eData.ListLevelUnlockID, ref GameData.Instance.Data.ListLevelUnlockID);
            GameData.Instance.LoadData(eData.DictLevelPlayInfoData, ref GameData.Instance.Data.DictLevelPlayInfoData);
            GameData.Instance.LoadData(eData.SkinSelectedID, ref GameData.Instance.Data.SkinSelectedID);
            GameData.Instance.LoadData(eData.TrySkinSelectedID, ref GameData.Instance.Data.TrySkinSelectedID);
            GameData.Instance.LoadData(eData.LastSkinSelectedID, ref GameData.Instance.Data.LastSkinSelectedID);
            GameData.Instance.LoadData(eData.ListSkinUnlockedID, ref GameData.Instance.Data.ListSkinUnlockedID);
            GameData.Instance.LoadData(eData.ListSkinAdsID, ref GameData.Instance.Data.ListSkinAdsID);
            GameData.Instance.LoadData(eData.Day, ref GameData.Instance.Data.Day);
            GameData.Instance.LoadData(eData.FirstDay, ref GameData.Instance.Data.FirstDay);
            GameData.Instance.LoadData(eData.IndexOfDayDaily, ref GameData.Instance.Data.IndexOfDayDaily);
            GameData.Instance.LoadData(eData.DayClamReward, ref GameData.Instance.Data.DayClamReward);
            GameData.Instance.LoadData(eData.StartAngleSpin, ref GameData.Instance.Data.StartAngleSpin);
            GameData.Instance.LoadData(eData.CanSpin, ref GameData.Instance.Data.CanSpin);
            GameData.Instance.LoadData(eData.TimeSpin, ref GameData.Instance.Data.TimeSpin);
            GameData.Instance.LoadData(eData.CountDownTimeSpin, ref GameData.Instance.Data.CountDownTimeSpin);
            GameData.Instance.LoadData(eData.ListIdSkinSpin, ref GameData.Instance.Data.ListIdSkinSpin);
            GameData.Instance.LoadData(eData.ProgressSkinVictoryCurrentCount, ref GameData.Instance.Data.ProgressSkinVictoryCurrentCount);
            GameData.Instance.LoadData(eData.ListIdSkinVictoryProgressDisplayed, ref GameData.Instance.Data.ListIdSkinVictoryProgressDisplayed);
            GameData.Instance.LoadData(eData.CurrentLanguage, ref GameData.Instance.Data.CurrentLanguage);
        }
    }
}
