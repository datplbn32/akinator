﻿using System;
using System.Collections.Generic;
using System.Globalization;
using GameTool;
using GameTool.Scripts;
using GameTool.Scripts.Data;
using UnityEngine;

namespace GameToolEditable.Data
{
    public class GameData : SingletonMonoBehaviour<GameData>
    {
        [Header("DATA INFO")]
        public GameDataSave Data;

        #region GAME CONFIG
        public static int TotalLevel = 50;
        #endregion

        #region ITEM INGAME CONFIG
        #endregion

        #region SKIN CONFIG
        #endregion

        #region DAILY REWARD

        public static int[] listTypeSkinDaily = { 3 };

        #endregion

        #region COROUTINE

        public static WaitForSeconds WaitFor1s { get; } = new WaitForSeconds(1f);

        public static WaitForSeconds WaitFor3s { get; } = new WaitForSeconds(3f);

        public static WaitForSecondsRealtime WaitFor1sRealtime { get; } = new WaitForSecondsRealtime(1f);

        #endregion

        #region FAKE
        public int CoinFake;
        public int DiamondFake;
        #endregion

        protected override void Awake()
        {
            base.Awake();
            LoadAllData();
        }

        #region LOAD DATA
        public void LoadAllData()
        {
            Data = new GameDataSave();
            GameDataControl.LoadAllData();

            OnAllDataLoaded();
        }
        #endregion

        #region SAVE DATA
        public void SaveAllData()
        {
            GameDataControl.SaveAllData();
        }
  
        public void SaveData<T>(eData filename, T value)
        {
            var save = new SaveUtility<T>();
            save.SaveData(filename, value);
        }

        public void LoadData<T>(eData filename, ref T variable)
        {
            var save = new SaveUtility<T>();
            save.LoadData(filename, ref variable);
        }

        public void OnAllDataLoaded()
        {
            Debug.Log("All Data Loaded!!!");
        }
        #endregion

        #region CLEAR DATA
        public void ClearData()
        {
            SaveGameManager.DeleteAllSave();
            Data = new GameDataSave();
            SaveAllData();
        }
        #endregion

        #region MORE DATA
        public bool FirstOpen
        {
            get => Data.FirstOpen;
            set
            {
                Data.FirstOpen = value;
                SaveData(eData.FirstOpen, Data.FirstOpen);
            }
        }

        public bool FirstPlay
        {
            get => Data.FirstPlay;
            set
            {
                Data.FirstPlay = value;
                SaveData(eData.FirstPlay, Data.FirstPlay);
            }
        }

        public bool Rated
        {
            get => Data.Rated;
            set
            {
                Data.Rated = value;
                SaveData(eData.Rated, Data.Rated);
            }
        }

        public bool RemoveAds
        {
            get => Data.RemoveAds;
            set
            {
                Data.RemoveAds = value;
                SaveData(eData.RemoveAds, Data.RemoveAds);
            }
        }

        public bool GDPR
        {
            get => Data.GDPR;
            set
            {
                Data.GDPR = value;
                SaveData(eData.GDPR, Data.GDPR);
            }
        }

        public bool GDPRShowed
        {
            get => Data.GDPRShowed;
            set
            {
                Data.GDPRShowed = value;
                SaveData(eData.GDPRShowed, Data.GDPRShowed);
            }
        }
        #endregion

        #region SETTING
        public bool MuteAll
        {
            get => Data.MuteAll;
            set
            {
                Data.MuteAll = value;
                SaveData(eData.MuteAll, Data.MuteAll);
            }
        }

        public bool Vibrate
        {
            get => Data.Vibrate;
            set
            {
                Data.Vibrate = value;
                SaveData(eData.Vibrate, Data.Vibrate);
            }
        }

        public bool Music
        {
            get => Data.Music;
            set
            {
                Data.Music = value;
                SaveData(eData.Music, Data.Music);
            }
        }

        public bool SoundFX
        {
            get => Data.SoundFX;
            set
            {
                Data.SoundFX = value;
                SaveData(eData.SoundFX, Data.SoundFX);
            }
        }

        public float MasterVolume
        {
            get => Data.MasterVolume;
            set
            {
                Data.MasterVolume = value;
                SaveData(eData.MasterVolume, Data.MasterVolume);
            }
        }

        public float MusicVolume
        {
            get => Data.MusicVolume;
            set
            {
                Data.MusicVolume = value;
                SaveData(eData.MusicVolume, Data.MusicVolume);
            }
        }

        public float SoundFXVolume
        {
            get => Data.SoundFXVolume;
            set
            {
                Data.SoundFXVolume = value;
                SaveData(eData.SoundFXVolume, Data.SoundFXVolume);
            }
        }
        #endregion

        #region GAMEPLAY
        public int VictoryCount
        {
            get => Data.VictoryCount;
            set
            {
                Data.VictoryCount = value;
                SaveData(eData.VictoryCount, Data.VictoryCount);
            }
        }

        public int LoseCount
        {
            get => Data.LoseCount;
            set
            {
                Data.LoseCount = value;
                SaveData(eData.LoseCount, Data.LoseCount);
            }
        }
        #endregion

        #region LEVEL  
        public int CurrentLevel
        {
            get => Data.CurrentLevel;
            set
            {
                Data.CurrentLevel = value;

                if (Data.CurrentLevel >= TotalLevel)
                {
                    Data.CurrentLevel = TotalLevel - 1;
                }

                if (value >= LevelUnlocked)
                {
                    LevelUnlocked = value;              
                }

                SaveData(eData.CurrentLevel, Data.CurrentLevel);
            }
        }

        public int LevelUnlocked
        {
            get => Data.LevelUnlocked;
            set
            {
                if (value >= TotalLevel)
                {
                    value = TotalLevel;
                }

                Data.LevelUnlocked = value;

                SaveData(eData.LevelUnlocked, Data.LevelUnlocked);
            }
        }

        public List<int> ListLevelUnlockID => Data.ListLevelUnlockID;

        public void UnlockLevel(int id)
        {
            if (!Data.ListLevelUnlockID.Contains(id))
            {
                Data.ListLevelUnlockID.Add(id);
                SaveData(eData.ListLevelUnlockID, Data.ListLevelUnlockID);
            }
        }

        public void LockLevel(int id)
        {
            if (Data.ListLevelUnlockID.Contains(id))
            {
                Data.ListLevelUnlockID.Remove(id);
                SaveData(eData.ListLevelUnlockID, Data.ListLevelUnlockID);
            }
        }

        public bool CheckLevelUnlock(int id)
        {
            if (Data.ListLevelUnlockID.Contains(id))
            {
                return true;
            }

            return false;
        }

        public LevelPlayInfoData GetLevelPlayInfoData(int id) 
        {
            if(Data.DictLevelPlayInfoData.ContainsKey(id)) 
            {
                return Data.DictLevelPlayInfoData[id];
            }

            LevelPlayInfoData dataNew = new LevelPlayInfoData(id);
            Data.DictLevelPlayInfoData.Add(id, dataNew);
            SaveData(eData.DictLevelPlayInfoData, Data.DictLevelPlayInfoData);
            return dataNew;
        }

        public void SetLevelPlayInfoData(LevelPlayInfoData dataValue) 
        {
            if (Data.DictLevelPlayInfoData.ContainsKey(dataValue.levelId))
            {
                Data.DictLevelPlayInfoData[dataValue.levelId] = dataValue;
                SaveData(eData.DictLevelPlayInfoData, Data.DictLevelPlayInfoData);
                return;
            }

            Data.DictLevelPlayInfoData.Add(dataValue.levelId, dataValue);
            SaveData(eData.DictLevelPlayInfoData, Data.DictLevelPlayInfoData);
        }
        #endregion

        #region SKIN
        public void UnlockSkin(int id)
        {
            if (!Data.ListSkinUnlockedID.Contains(id))
            {
                Data.ListSkinUnlockedID.Add(id);
                SaveData(eData.ListSkinUnlockedID, Data.ListSkinUnlockedID);
            }
        }

        public void LockSkin(int id)
        {
            if (Data.ListSkinUnlockedID.Contains(id))
            {
                Data.ListSkinUnlockedID.Remove(id);
                SaveData(eData.ListSkinUnlockedID, Data.ListSkinUnlockedID);
            }
        }

        public bool CheckSkinUnlock(int id)
        {
            if (Data.ListSkinUnlockedID.Contains(id))
            {
                return true;
            }

            return false;
        }

        public int SkinSelectedID
        {
            get => Data.SkinSelectedID;
            set
            {
                if(Data.SkinSelectedID != value)
                {
                    Data.SkinSelectedID = value;
                    SaveData(eData.SkinSelectedID, Data.SkinSelectedID);
                }
            }
        }

        public int TrySkinSelectedID
        {
            get => Data.TrySkinSelectedID;
            set
            {
                if (Data.TrySkinSelectedID != value)
                {
                    Data.TrySkinSelectedID = value;
                    SaveData(eData.TrySkinSelectedID, Data.TrySkinSelectedID);
                }
            }
        }

        public bool CheckSkinSelected(int id)
        {
            if (id == SkinSelectedID)
            {
                return true;
            }
            return false;
        }

        public List<int> ListIdSkinLock()
        {
            List<int> listId = new List<int>();

            listId.Clear();
#if USE_FIREBASE
        foreach (int id in FirebaseRemote.Instance.GetSkinConfig().ListTotalSkin)
        {
            if (!CheckSkinUnlock(id))
            {
                listId.Add(id);
            }
        }
#endif  
            return listId;
        }
        #endregion

        #region DAILY REWARD
        public int Day
        {
            get => Data.Day;
            set
            {
                Data.Day = value;
                SaveData(eData.Day, Data.Day);
            }
        }

        public int FirstDay
        {
            get => Data.FirstDay;
            set
            {
                Data.FirstDay = value;
                SaveData(eData.FirstDay, Data.FirstDay);
            }
        }

        public int IndexOfDayDaily
        {
            get => Data.IndexOfDayDaily;
            set
            {
                Data.IndexOfDayDaily = value;
                SaveData(eData.IndexOfDayDaily, Data.IndexOfDayDaily);
            }
        }

        public int DayClamReward
        {
            get => Data.DayClamReward;
            set
            {
                Data.DayClamReward = value;
                SaveData(eData.DayClamReward, Data.DayClamReward);
            }
        }
        #endregion
        #region SPIN
        public bool CanSpin
        {
            get => Data.CanSpin;
            set
            {
                Data.CanSpin = value;
                SaveData(eData.CanSpin, Data.CanSpin);
            }
        }

        public float StartAngleSpin
        {
            get => Data.StartAngleSpin;
            set
            {
                Data.StartAngleSpin = value;
                SaveData(eData.StartAngleSpin, Data.StartAngleSpin);
            }
        }

        public float CountDownTimeSpin
        {
            get => Data.CountDownTimeSpin;
            set
            {
                Data.CountDownTimeSpin = value;
                SaveData(eData.CountDownTimeSpin, Data.CountDownTimeSpin);
            }
        }
        public List<int> ListIdSkinSpin => Data.ListIdSkinSpin;

        public DateTime TimeSpin
        {
            get => DateTime.Parse(Data.TimeSpin, CultureInfo.InvariantCulture);
            set
            {
                Data.TimeSpin = value.ToString(CultureInfo.InvariantCulture);
                SaveData(eData.TimeSpin, Data.TimeSpin);
            }
        }
        #endregion

        #region SKIN PROGRESS VICTORY
        public int ProgressSkinVictoryCurrentCount
        {
            get => Data.ProgressSkinVictoryCurrentCount;
            set
            {
                Data.ProgressSkinVictoryCurrentCount = value;
                SaveData(eData.ProgressSkinVictoryCurrentCount, Data.ProgressSkinVictoryCurrentCount);
            }
        }

        public bool IsSkinVictoryProgressDisplayed(int id)
        {
            return Data.ListIdSkinVictoryProgressDisplayed.Contains(id);
        }

        public void SetSkinVictoryProgressDisplay(int id)
        {
            if (!IsSkinVictoryProgressDisplayed(id))
            {
                Data.ListIdSkinVictoryProgressDisplayed.Add(id);
                SaveData(eData.ListIdSkinVictoryProgressDisplayed, Data.ListIdSkinVictoryProgressDisplayed);
            }
        }

        public void ClearSkinVictoryProgressDisplay()
        {
            Data.ListIdSkinVictoryProgressDisplayed.Clear();
            SaveData(eData.ListIdSkinVictoryProgressDisplayed, Data.ListIdSkinVictoryProgressDisplayed);
        }
        #endregion

        #region LANGUAGE
        public int CurrenLanguage
        {
            get => Data.CurrentLanguage;
            set
            {
                Data.CurrentLanguage = value;
                SaveData(eData.CurrentLanguage, Data.CurrentLanguage);
            }
        }
        #endregion

    }

    [Serializable]
    public class GameDataSave
    {
        [Header("GAME CHECK")]
        public bool FirstOpen;
        public bool FirstPlay;
        public bool Rated;
        public bool RemoveAds;

        public bool GDPR = true;
        public bool GDPRShowed;

        [Header("SETTING")]
        public bool MuteAll;

        public bool Music = true;
        public bool SoundFX = true;
        public bool Vibrate = true;

        public float MasterVolume = 1f;
        public float MusicVolume = 1f;
        public float SoundFXVolume = 1f;

        [Header("RESOURCES")]
        public int Coin;
        public int Diamond;

        [Header("GAMEPLAY")]
        public int VictoryCount;
        public int LoseCount;

        [Header("LEVEL")]
        public int CurrentLevel;
        public int LevelUnlocked;
        public List<int> ListLevelUnlockID = new List<int>() { 0 };
        public Dict<int, LevelPlayInfoData> DictLevelPlayInfoData = new Dict<int, LevelPlayInfoData>();

        [Header("SKIN")]
        public int SkinSelectedID;
        public int TrySkinSelectedID = -1;
        public int LastSkinSelectedID;
        public List<int> ListSkinUnlockedID = new List<int>() { 0 };
        public List<SkinAdsProgress> ListSkinAdsID = new List<SkinAdsProgress>();

        [Header("DAILY REWARD")]
        public int Day;
        public int FirstDay;
        public int IndexOfDayDaily;
        public int DayClamReward;

        [Header("SPIN")] 
        public float StartAngleSpin;
        public bool CanSpin;
        public string TimeSpin = DateTime.Now.ToString(CultureInfo.InvariantCulture);
        public float CountDownTimeSpin = -1;
        public List<int> ListIdSkinSpin = new List<int>();

        [Header("SKIN VICTORY PROGRESS")]
        public int ProgressSkinVictoryCurrentCount;
        public List<int> ListIdSkinVictoryProgressDisplayed = new List<int>();

        [Header("LANGUAGE")]
        public int CurrentLanguage;
    }

    [Serializable]
    public class SkinAdsProgress
    {
        public int id;
        public int currentProgress;

        public SkinAdsProgress(int id = 0, int currentProgress = 0)
        {
            this.id = id;
            this.currentProgress = currentProgress;
        }
    }

    [Serializable]
    public class LevelPlayInfoData 
    {
        public int levelId;
        public int playCount;
        public int victoryCount;
        public int loseCount;
        public int deadCount;
        public int reviveCount;
        public int skipCount;
        public int score;

        public LevelPlayInfoData(int levelId = 0, int playCount = 0, int victoryCount = 0, int loseCount = 0, int deadCount = 0, int reviveCount = 0, int skipCount = 0, int score = 0)
        {
            this.levelId = levelId;
            this.playCount = playCount;
            this.victoryCount = victoryCount;
            this.loseCount = loseCount;
            this.deadCount = deadCount;
            this.reviveCount = reviveCount;
            this.skipCount = skipCount;
            this.score = score;
        }
    }
}