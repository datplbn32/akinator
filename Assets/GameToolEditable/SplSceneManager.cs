using System.Collections;
using GameTool;
using GameTool.Scripts;
using UnityEngine;

namespace GameToolEditable
{
    public class SplSceneManager : SplSceneManagerBase
    {
        protected override IEnumerator LoadSceneHome()
        {
            yield return new WaitForSeconds(3);
            LoadSceneManager.Instance.LoadSceneHome();
        }
    }
}
