namespace GameToolEditable.Pooling
{
    public enum ePrefabPool
    {
        None,
        ItemRewardUIShow,
        ItemShowUIPrefab,
        ItemShowSpritePrefab,
    }
}
