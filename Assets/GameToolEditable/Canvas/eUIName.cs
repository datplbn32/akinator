namespace GameToolEditable.Canvas
{
    public enum eUIName
    {
        None,
        HomeMenu,
        GamePlayMenu,
        SettingPopup,
        VictoryPopup,
        LosePopup,
        RevivePopup,
        SpinPopup,
        RewardPopup,
        SkinPopup,
        DailyRewardPopup,
        ShopPopup,
    }
}
